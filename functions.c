#include "header.h"
#include "functions.h"

void test(fifo_queue * test_queue){
    for(int i = 0; i < 10; ++i){
        add_element(i, test_queue);
    }

    show_queue(test_queue);
    printf("%d %d %d\n", check_first_out_value(test_queue), count_odd(test_queue), count_even(test_queue));

    for(int i = 0; i < 5; ++i)
        delete_element(test_queue);

    show_queue(test_queue);
    printf("%d %d %d\n", check_first_out_value(test_queue), count_odd(test_queue), count_even(test_queue));
}

int generate_even(){
    int i = rand();
    while(i % 2)
        i = rand();
    return i % 100;
}

int generate_odd(){
    int i = rand();
    while(!(i % 2))
        i = rand();
    return i % 100;
}

bool even_can_produce(fifo_queue * queue){
    if(count_even(queue) < 10)
        return true;
    return false;
}

bool odd_can_produce(fifo_queue * queue){
    if(count_even(queue) > count_odd(queue))
        return true;
    return false;
}

bool even_can_consume(fifo_queue * queue){
    if((count_even(queue) + count_odd(queue) > 2) && !(check_first_out_value(queue) % 2))
        return true;
    return false;
}

bool odd_can_consume(fifo_queue * queue){
    if((count_even(queue) + count_odd(queue) > 6) && (check_first_out_value(queue) % 2))
        return true;
    return false;
}
