#ifndef FIFO_QUEUE_H
#define FIFO_QUEUE_H

#include <stdbool.h>

#define MAX 20

typedef struct{
    int buffer[MAX];
    int first_element;
    int last_element;
    bool full;
}   fifo_queue;

fifo_queue * get_new_queue();

void add_element(int new_value, fifo_queue * queue);

void delete_element(fifo_queue * queue);

int check_first_out_value(fifo_queue * queue);

int count_even(fifo_queue * queue);

int count_odd(fifo_queue * queue);

void delete_queue(fifo_queue * queue);

void show_queue(fifo_queue * queue);

#endif