#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdbool.h>
#include "fifo_queue.h"

void test(fifo_queue * shared_queue);

int generate_even();

int generate_odd();

bool even_can_produce(fifo_queue * queue);

bool odd_can_produce(fifo_queue * queue);

bool even_can_consume(fifo_queue * queue);

bool odd_can_consume(fifo_queue * queue);

#endif