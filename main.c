#include "header.h"
#include "fifo_queue.h"
#include "functions.h"

sem_t critical_section_mutex;
sem_t even_producer_mutex;
sem_t odd_producer_mutex;
sem_t even_consumer_mutex; 
sem_t odd_consumer_mutex;

int even_producer_waiting;
int odd_producer_waiting;
int even_consumer_waiting;
int odd_consumer_waiting;

void * produce_even_number(void * args);

void * produce_odd_number(void * args);

void * consume_even_number(void * args);

void * consume_odd_number(void * args);

int main(void){
    srand(time(NULL));
    fifo_queue * shared_queue = get_new_queue();

    sem_init(&critical_section_mutex, 0, 1);
    sem_init(&even_producer_mutex, 0, 1);
    sem_init(&odd_producer_mutex, 0, 0);
    sem_init(&even_consumer_mutex, 0, 0);
    sem_init(&odd_consumer_mutex, 0, 0);

    even_producer_waiting = 0;
    odd_producer_waiting = 0;
    even_consumer_waiting = 0;
    odd_consumer_waiting = 0;

    pthread_t even_producer_thread, odd_producer_thread, even_consumer_thread, odd_consumer_thread;

    pthread_create(&even_producer_thread, NULL, produce_even_number, (void *) shared_queue);
    pthread_create(&odd_producer_thread, NULL, produce_odd_number, (void *) shared_queue);
    pthread_create(&even_consumer_thread, NULL, consume_even_number, (void *) shared_queue);
    pthread_create(&odd_consumer_thread, NULL, consume_odd_number, (void *) shared_queue);

    pthread_join(even_producer_thread, NULL);
    pthread_join(odd_producer_thread, NULL);
    pthread_join(even_consumer_thread, NULL);
    pthread_join(odd_consumer_thread, NULL);
    
    delete_queue(shared_queue);
    sem_destroy(&critical_section_mutex);
    sem_destroy(&even_producer_mutex);
    sem_destroy(&odd_producer_mutex);
    sem_destroy(&even_consumer_mutex);
    sem_destroy(&odd_consumer_mutex);

    return 0;
}

void * produce_even_number(void * args){
    fifo_queue * shared_queue = (fifo_queue *) args;
    while(true){
        sem_wait(&critical_section_mutex);
        
        if(!even_can_produce(shared_queue)){

            ++even_producer_waiting;

            printf("In even producer waiting, number = %d.\n", even_producer_waiting);

            sem_post(&critical_section_mutex);
            sem_wait(&even_producer_mutex);

            --even_producer_waiting;
        }

            printf("In even producer operation - status = even = %d, odd = %d.\n",
                count_even(shared_queue), count_odd(shared_queue));

            add_element(generate_even(), shared_queue);
            show_queue(shared_queue);

            if(odd_can_consume(shared_queue) && odd_consumer_waiting)
                sem_post(&odd_consumer_mutex);
            else if(odd_can_produce(shared_queue) && odd_producer_waiting)
                sem_post(&odd_producer_mutex);
            else if(even_can_consume(shared_queue) && even_consumer_waiting)
                sem_post(&even_consumer_mutex);
            else 
                sem_post(&critical_section_mutex);

            sleep(1);
    }
    printf("Even producer end.\n");
}

void * produce_odd_number(void * args){
    fifo_queue * shared_queue = (fifo_queue *) args;
    while(true){
        sem_wait(&critical_section_mutex);
        
        if(!odd_can_produce(shared_queue)){

            ++odd_producer_waiting;

            printf("In odd producer waiting, number = %d.\n", odd_producer_waiting);

            sem_post(&critical_section_mutex);
            sem_wait(&odd_producer_mutex);

            --odd_producer_waiting;
        }

            printf("In odd producer operation. - status = even = %d, odd = %d.\n",
                count_even(shared_queue), count_odd(shared_queue));

            add_element(generate_odd(), shared_queue);
            show_queue(shared_queue);
            
            if(even_can_produce(shared_queue) && even_producer_waiting)
                sem_post(&even_producer_mutex);
            else if(even_can_consume(shared_queue) && even_consumer_waiting)
                sem_post(&even_consumer_mutex);
            else if(odd_can_consume(shared_queue) && odd_consumer_waiting)
                sem_post(&odd_consumer_mutex);
            else 
                sem_post(&critical_section_mutex);

            sleep(1);
        
    }
    printf("Odd producer end.\n");
}

void * consume_even_number(void * args){
    fifo_queue * shared_queue = (fifo_queue *) args;
    while(true){
        sem_wait(&critical_section_mutex);
        
        if(!even_can_consume(shared_queue)){

            ++even_consumer_waiting;

            printf("In even consumer waiting, number = %d.\n", even_consumer_waiting);

            sem_post(&critical_section_mutex);
            sem_wait(&even_consumer_mutex);

            --even_consumer_waiting;
        }

        sleep(1);

        printf("In even consumer operation. - status = even = %d, odd = %d, to_delete = %d.\n",
            count_even(shared_queue), count_odd(shared_queue), check_first_out_value(shared_queue));

        delete_element(shared_queue);
        show_queue(shared_queue);
        
        if(odd_can_produce(shared_queue) && odd_producer_waiting)
            sem_post(&odd_producer_mutex);
        else if(odd_can_consume(shared_queue) && odd_consumer_waiting)
            sem_post(&odd_consumer_mutex);
        else if(even_can_produce(shared_queue) && even_producer_waiting)
            sem_post(&even_producer_mutex);
        else 
            sem_post(&critical_section_mutex);
    }
    printf("Even consumer end.\n");
}

void * consume_odd_number(void * args){
    fifo_queue * shared_queue = (fifo_queue *) args;
    while(true){
        sem_wait(&critical_section_mutex);
        
        if(!odd_can_consume(shared_queue)){

            ++odd_consumer_waiting;

            printf("In odd consumer waiting, number = %d.\n", odd_consumer_waiting);

            sem_post(&critical_section_mutex);
            sem_wait(&odd_consumer_mutex);

            --odd_consumer_waiting;
        }

            sleep(1);

            printf("In odd consumer operation - status = even = %d, odd = %d, to_delete = %d.\n",
                count_even(shared_queue), count_odd(shared_queue), check_first_out_value(shared_queue));

            delete_element(shared_queue);
            show_queue(shared_queue);
            
            if(even_can_produce(shared_queue) && even_producer_waiting)
                sem_post(&even_producer_mutex);
            else if(odd_can_produce(shared_queue) && odd_producer_waiting)
                sem_post(&odd_producer_mutex);
            else if(even_can_consume(shared_queue) && even_consumer_waiting)
                sem_post(&even_consumer_mutex);
            else 
                sem_post(&critical_section_mutex);
    }
    printf("Odd consumer end.\n");
}